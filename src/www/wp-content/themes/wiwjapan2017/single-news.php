<?php get_header(); ?>
<section class="l-section page-header news-header">
	<div class="l-section__inner page-header__inner news-header__inner">
		<a href="/news/" class="page-header__back news-header__back">News</a>
		<?php
		while ( have_posts() ) : the_post();
		?>
		<p class="date"><?php echo get_post_time('Y.m.d D'); ?></p>
		<h1 class="title"><?php the_title(); ?></h1>
	</div>
</section>
<section class="l-section page-contents news-article">
	<div class="l-section__inner page-contents__inner news-article__inner">
		<div class="news-main">
			<div class="news-main__body news-main-body is-news">
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</section>
<?php
	endwhile;
?>
<?php get_footer(); ?>
