<?php wp_deregister_script('jquery'); ?><!DOCTYPE html>
<html <?php language_attributes(); ?> prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="description" content="">
<title><?php wp_title(' | ',true,'right'); ?><?php bloginfo('name'); ?></title>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-76492205-1', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');
</script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> id="body">
<div id="spFlg"></div>
  <header class="l-header header" id="header">
    <div class="l-header__inner">
      <h1 class="header-logo">
        <a href="/" class="header-logo__link">
          <svg role="image" class="header-logo__logo svg-icon">
            <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/icons.svg#logo_horizontal" />
          </svg>
        </a>
      </h1>
      <a href="#" class="toggle-menu show-sp" id="js-toggle-menu">
        <span class="toggle-menu__btn"></span>

      </a>
      <nav class="header-menu" id="js-header-menu">
        <div class="header-menu__section g-nav">
          <a href="/about/" class="g-nav__item">About</a>
          <a href="/events/" class="g-nav__item">Events</a>
          <a href="/news/" class="g-nav__item">News</a>
          <a href="/2016/" class="g-nav__item" target="_blank">Archive</a>
          <a href="/form_contact/" class="g-nav__item">Contact</a>
        </div>
        <div class="header-menu__section sub-nav">
          <a href="https://www.facebook.com/World-Interiors-Day-1614209928791677/" class="sub-nav__item is-icon" target="_blank">
            <svg role="image" class="svg-icon is-facebook">
              <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/icons.svg#icon_facebook" />
            </svg>
          </a>

          <a href="/about_en" class="sub-nav__item">English</a>

        </div>
      </nav>
    </div>
  </header>
  <div class="l-wrapper">
    <main class="l-main" id="main">
