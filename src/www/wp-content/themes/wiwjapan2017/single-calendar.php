<?php get_header(); ?>
<section class="l-section page-header event-header">
  <div class="l-section__inner page-header__inner event-header__inner">
    <a href="/calendar/" class="page-header__back event-header__back">Event Calendar</a>
    <?php
      while ( have_posts() ) : the_post();
    ?>
    <p class="date">
      <?php
        $startDate = get_field('start_date');
        $endDate = get_field('end_date');
        echo date('Y.n.j D', strtotime($startDate));
        if($startDate !== $endDate) {
          echo ' - ';
          echo date('Y.n.j D', strtotime($endDate));
        }
      ?>
    </p>
    <h1 class="title"><?php the_title(); ?></h1>
    <?php if( get_field('place_1') ): ?>
    <span class="place">at <?php the_field('place_1'); ?></span>
    <?php endif; ?>
    <?php if( get_field('place_2') ): ?>
    <span class="place">at <?php the_field('place_2'); ?></span>
    <?php endif; ?>
    <?php if( get_field('place_3') ): ?>
    <span class="place">at <?php the_field('place_3'); ?></span>
    <?php endif; ?>
  </div>
</section>
<section class="l-section page-contents event-article">
  <div class="l-section__inner page-contents__inner event-article__inner">
    <div class="event-detail">
      <div class="event-detail__body event-detail-body is-calendar">
        <?php the_content(); ?>
        <?php if( get_field('pdf') ): ?>
        <div class="event-files">
          <a href="<?php the_field('pdf'); ?>" class="event-files__item" target="_blank">ご案内<span class="file-type">PDF</span></a>
        </div>
        <?php endif; ?>
        <?php if( get_field('website') ): ?>
        <a href="<?php the_field('website'); ?>" class="event-detail__link button" target="_blank"><span>Website</span></a>
        <?php endif; ?>
      </div>
      <div class="event-detail__info event-info">
        <h2 class="event-info__title">Access</h2>
        <div class="event-info__body event-info-body">
          <div class="event-info-body__item">
            <div class="event-address">
              <?php if( get_field('place_1') ): ?>
              <h3 class="name"><?php the_field('place_1'); ?></h3>
              <?php endif; ?>
              <?php if( get_field('address_1') ): ?>
              <p class="address"><?php the_field('address_1'); ?></p>
              <?php endif; ?>
              <?php
                $map = get_field('map_1');
                if( !empty($map) ):
              ?>
              <div class="acf-map">
                <div class="marker" data-lat="<?php echo $map['lat']; ?>" data-lng="<?php echo $map['lng']; ?>"></div>
              </div>
              <?php endif; ?>
              <?php if( get_field('place_2') ): ?>
              <h3 class="name"><?php the_field('place_2'); ?></h3>
              <?php endif; ?>
              <?php if( get_field('address_2') ): ?>
              <p class="address"><?php the_field('address_2'); ?></p>
              <?php endif; ?>
              <?php
                $map = get_field('map_2');
                if( !empty($map) ):
              ?>
              <div class="acf-map">
                <div class="marker" data-lat="<?php echo $map['lat']; ?>" data-lng="<?php echo $map['lng']; ?>"></div>
              </div>
              <?php endif; ?>
              <?php if( get_field('place_3') ): ?>
              <h3 class="name"><?php the_field('place_3'); ?></h3>
              <?php endif; ?>
              <?php if( get_field('address_3') ): ?>
              <p class="address"><?php the_field('address_3'); ?></p>
              <?php endif; ?>
              <?php
                $map = get_field('map_3');
                if( !empty($map) ):
              ?>
              <div class="acf-map">
                <div class="marker" data-lat="<?php echo $map['lat']; ?>" data-lng="<?php echo $map['lng']; ?>"></div>
              </div>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
  endwhile;
?>
<?php get_footer(); ?>
