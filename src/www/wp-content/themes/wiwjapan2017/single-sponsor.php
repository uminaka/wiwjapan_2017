<?php get_header(); ?>
<?php
while ( have_posts() ) : the_post();
?>
<section class="l-section page-header sponsor-header">
  <div class="l-section__inner page-header__inner sponsor-header__inner">
    <h1 class="title"><?php the_title(); ?></h1>
  </div>
</section>
<section class="l-section page-contents sponsor-article">
  <div class="l-section__inner page-contents__inner sponsor-article__inner">
    <div class="sponsor-detail">
      <div class="sponsor-detail__body sponsor-detail-body">
        <?php the_content(); ?>
      </div>
      <div class="sponsor-detail__info sponsor-info">
        <h2 class="sponsor-info__title">Company Information</h2>
        <div class="sponsor-info__body sponsor-info-body">
          <div class="sponsor-info-body__item">
            <div class="sponsor-address">
              <?php if( get_field('place_1') ): ?>
              <h3 class="name"><?php the_field('place_1'); ?></h3>
              <?php endif; ?>
              <?php if( get_field('address_1') ): ?>
              <p class="address"><?php the_field('address_1'); ?></p>
              <?php endif; ?>
              <?php if( get_field('tel_1') ): ?>
              <p class="address"><?php the_field('tel_1'); ?></p>
              <?php endif; ?>
              <?php
                $map = get_field('map_1');
                if( !empty($map) ):
              ?>
              <div class="acf-map">
                <div class="marker" data-lat="<?php echo $map['lat']; ?>" data-lng="<?php echo $map['lng']; ?>"></div>
              </div>
              <?php endif; ?>
              <?php if( get_field('place_2') ): ?>
              <h3 class="name"><?php the_field('place_2'); ?></h3>
              <?php endif; ?>
              <?php if( get_field('address_2') ): ?>
              <p class="address"><?php the_field('address_2'); ?></p>
              <?php endif; ?>
              <?php if( get_field('tel_2') ): ?>
              <p class="address"><?php the_field('tel_2'); ?></p>
              <?php endif; ?>
              <?php
                $map = get_field('map_2');
                if( !empty($map) ):
              ?>
              <div class="acf-map">
                <div class="marker" data-lat="<?php echo $map['lat']; ?>" data-lng="<?php echo $map['lng']; ?>"></div>
              </div>
              <?php endif; ?>
              <?php if( get_field('website') ): ?>
              <a href="<?php the_field('website'); ?>" class="sponsor-detail-link button" target="_blank"><span>Website</span></a>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
  endwhile;
?>
<?php get_footer(); ?>
