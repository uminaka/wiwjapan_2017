<?php
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
  wp_enqueue_style( 'main-style', get_template_directory_uri() . '/css/style.css');
}

// アイキャッチ画像
add_theme_support('post-thumbnails');
set_post_thumbnail_size(506, 314, true);
add_image_size('article', 720, 9999);
add_image_size('banner A', 506, 9999);
add_image_size('banner B', 239, 9999);

$headerArgs = array(
'width' => 720 ,
'height' => 720,
'header-text' => false,
);
add_theme_support('custom-header', $headerArgs);

// カスタム投稿タイプ作成
function create_post_type_calendar() {
  $exampleSupports = [
    'title',
    'editor',
    'thumbnail',
    'revisions'
  ];

  // add post type
  register_post_type( 'calendar',
    array(
      'label' => 'イベントカレンダー',
      'public' => true,
      'has_archive' => true,
      'menu_position' => 6,
      'supports' => $exampleSupports
    )
  );
}

function create_post_type_news() {
  $exampleSupports = [
    'title',
    'editor',
    'thumbnail',
    'revisions'
  ];

  // add post type
  register_post_type( 'news',
    array(
      'label' => 'ニュース',
      'public' => true,
      'has_archive' => true,
      'menu_position' => 5,
      'supports' => $exampleSupports
    )
  );
}

function create_post_type_sponsor() {
  $exampleSupports = [
    'title',
    'editor',
    'thumbnail',
    'revisions'
  ];

  // add post type
  register_post_type( 'sponsor',
    array(
      'label' => '広告スポンサー',
      'public' => true,
      'has_archive' => false,
      'menu_position' => 7,
      'supports' => $exampleSupports
    )
  );

  // add taxonomy
  register_taxonomy(
    'sponsor_taxonomy',
    'sponsor',
    array(
      'label' => '広告タイプ (A/B)',
      'labels' => array(
        'all_items' => 'タクソノミー一覧',
        'add_new_item' => '新規タクソノミーを追加'
      ),
      'hierarchical' => true
    )
  );
}

add_action( 'init', 'create_post_type_calendar' );
add_action( 'init', 'create_post_type_news' );
add_action( 'init', 'create_post_type_sponsor' );

function my_acf_google_map_api( $api ){

	$api['key'] = 'AIzaSyAg54N9LTZJpbLVUKn-THCNkEX3bTeSTbE';

	return $api;

}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
