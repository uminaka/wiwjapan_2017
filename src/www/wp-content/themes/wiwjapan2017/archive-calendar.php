<?php get_header(); ?>
<section class="l-section page-header">
	<div class="l-section__inner page-header__inner">
		<h1 class="page-header__title">Event Calendar</h1>
	</div>
</section>
<section class="l-section page-contents">
	<div class="l-section__inner page-contents__inner">
		<div class="calendar-list">
			<a href="/events/event-1" class="calendar-list__item calendar-list-item">
				<img src="<?php bloginfo('template_url'); ?>/images/events-list_01.jpg" alt="" class="calendar-list-item__image">
				<span class="calendar-list-item__text">
					<span class="info">
						<span class="date">2017.5.25-31</span>
					</span>
					<span class="title">Interior Design for Generations</span>
				</span>
			</a>
			<a href="/events/event-3" class="calendar-list__item calendar-list-item">
				<img src="<?php bloginfo('template_url'); ?>/images/events-list_03.jpg" alt="" class="calendar-list-item__image">
				<span class="calendar-list-item__text">
					<span class="info">
						<span class="date">2017.5.26-6.3</span>
					</span>
					<span class="title">暮らしの未来-Next Generationインテリアデザイナー展</span>
				</span>
			</a>
			<a href="/events/event-2" class="calendar-list__item calendar-list-item">
				<img src="<?php bloginfo('template_url'); ?>/images/events-list_02.jpg" alt="" class="calendar-list-item__image">
				<span class="calendar-list-item__text">
					<span class="info">
						<span class="date">2017.5.26</span>
					</span>
					<span class="title">2017年国際展示会・デザイントレンドに関する情報セミナー</span>
				</span>
			</a>
			<a href="/events/event-4" class="calendar-list__item calendar-list-item">
				<img src="<?php bloginfo('template_url'); ?>/images/events-list_04.jpg" alt="" class="calendar-list-item__image">
				<span class="calendar-list-item__text">
					<span class="info">
						<span class="date">2017.5.27</span>
					</span>
					<span class="title">「World Interiors Day」デザインシンポジウム &amp; ワインパーティ</span>
				</span>
			</a>
			<a href="/events/event-2" class="calendar-list__item calendar-list-item">
				<img src="<?php bloginfo('template_url'); ?>/images/calendar_placeholder.png" alt="" class="calendar-list-item__image">
				<span class="calendar-list-item__text">
					<span class="info">
						<span class="date">2017.5.27-6.3</span>
					</span>
					<span class="title">毎日の暮らしで使いたい『インテリア小物をつくろう』</span>
				</span>
			</a>
			<?php if (have_posts()): ?>
			<?php while (have_posts()) : the_post(); ?>
			<a href="<?php the_permalink(); ?>" class="calendar-list__item calendar-list-item">
				<?php if (has_post_thumbnail()) : ?>
					<?php the_post_thumbnail('thumbnail', array( 'class' => 'calendar-list-item__image' ) ); ?>
				<?php else : ?>
					<img src="<?php bloginfo('template_url'); ?>/images/calendar_placeholder.png" alt="" class="calendar-list-item__image">
				<?php endif ; ?>
				<span class="calendar-list-item__text">
					<span class="info">
						<span class="date"><?php echo get_field('time'); ?></span>
					</span>
					<span class="title"><?php echo get_the_title(); ?></span>
				</span>
			</a>
			<?php endwhile; ?>
			<?php else: ?>
			<?php endif; ?>
		</div>
		<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
	</div>
</section>
<?php get_footer(); ?>
