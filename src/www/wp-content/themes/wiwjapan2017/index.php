<?php get_header(); ?>

    <section class="l-section kv">
      <div class="kv__image slider js-kv-slider kv-slider">
        <?php
          $allheaderimg = get_uploaded_header_images();
          if(count($allheaderimg) >= 1) {
            foreach($allheaderimg as $headerimg) {
              echo '<div class="slider__item kv-slider__item kv-slider-item"><div class="kv-slider-item__inner"><img src="' . $headerimg['url'] . '" class="kv-slider-item__image" alt=""></div></div>';
            }
          } else {

          }
        ?>
      </div>
      <div class="kv__text">

        <h2 class="image-wrapper is-catch">
          <img src="<?php echo get_template_directory_uri(); ?>/images/catch.svg" alt="「人」と「暮らし」を「デザイン」でつなぐ国際イベント">
        </h2>
        <div class="image-wrapper is-title_theme is-title-label">
          2017年世界共通テーマ

        </div>
        <h3 class="image-wrapper is-theme">
          <img src="<?php echo get_template_directory_uri(); ?>/images/theme.svg" alt="Interior Design for Generations">
        </h3>
        <div class="image-wrapper is-date">
          <img src="<?php echo get_template_directory_uri(); ?>/images/date.svg" alt="2017年5月27日（土）〜5月28日（日）">
        </div>
        <div class="image-wrapper is-title_place_main is-title-label">
          メイン会場

        </div>
        <div class="image-wrapper is-place_main">
          <img src="<?php echo get_template_directory_uri(); ?>/images/place_main.svg" alt="東京ミッドタウン・デザインハブ">
        </div>
        <div class="image-wrapper is-title_place_area is-title-label">
          エリアイベント

        </div>
        <div class="image-wrapper is-place_area">
          <img src="<?php echo get_template_directory_uri(); ?>/images/place_area.svg" alt="札幌、新潟、東京、名古屋、大阪、神戸、福岡">
        </div>
        <div class="image-wrapper is-title_place_events is-title-label">
          参加型自主イベント会場

        </div>
        <div class="image-wrapper is-place_events">
          <img src="<?php echo get_template_directory_uri(); ?>/images/place_events.svg" alt="国内各地">
          <a href="/form_entry_1/" class="kv-entry link-arrow"><span>Entry</span></a>
        </div>
      </div>
    </section>
    <section class="l-section">
      <div class="l-section__inner intro">
        <div class="intro__logo">
          <svg role="image" class="svg-icon">
            <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/icons.svg#logo" />
          </svg>
        </div>
        <div class="intro__body">
          <h3 class="headline">ワールドインテリアウィーク</h3>
          <p>インテリアデザインが、社会、文化、私たちの将来の生活環境に果たす役割について、皆様と共に積極的に理解を深める運動です。</p>
          <p>IFI (国際インテリアアーキテクト/デザイナー団体連合) が推奨している世界同時イベント<strong>「World Interiors Day」</strong>を受けて、IFIメンバーである JID と JDP は、この活動を支援すると共に日本ではさらに拡張させ毎年5月最終土曜日を含めた1週間を、インテリアデザインと暮らしを考える週として「World Interiors Week in JAPAN」と称し広範囲な運動を展開します。</p>
        </div>
      </div>
    </section>
    <?php /*
    <section class="l-section top-news">
      <div class="top-news__list top-news-list">
        <?php
        $args = array(
          'post_type' => 'news',
          'post_status' => 'publish',
          'posts_per_page' => 6
        );
        $the_query = new WP_Query($args); if($the_query->have_posts()):
        ?>
        <?php while ($the_query->have_posts()): $the_query->the_post(); ?>
        <a href="<?php the_permalink(); ?>" class="top-news-list__item top-news-item">
          <?php if (has_post_thumbnail()) : ?>
            <?php the_post_thumbnail('thumbnail', array( 'class' => 'image' ) ); ?>
          <?php else : ?>
            <img src="<?php bloginfo('template_url'); ?>/images/news_placeholder.png" alt="" class="image">
          <?php endif ; ?>
          <span class="info">
            <span class="date"><?php echo get_post_time('Y.n.d D'); ?></span>
            <span class="title"><?php echo get_the_title(); ?></span>
          </span>
        </a>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
        <?php else: ?>
        <!-- 投稿が無い場合の処理 -->
        <?php endif; ?>
      </div>
      <a href="/news/" class="top-news__more link-arrow"><span>News Topics</span></a>
    </section>
    */ ?>
    <section class="l-section">
      <div class="slider js-event-slider event-slider">
        <div class="slider__item event-slider__item event-slider-item">
          <div class="event-slider-item__inner">
            <img src="<?php echo get_template_directory_uri(); ?>/images/events-list_01.jpg" class="event-slider-item__image">
            <div class="event-slider-item__text">
              <p class="note">企画参加募集中</p>
              <h2 class="title">Event 1</h2>
              <h3 class="sub-title">参加型自主イベント<br>地域ネットワークイベント</h3>
              <p class="desc">
                みんなでつくる参加型イベント<br>
                IFI共通のテーマを掲げ全国運動を世界と共通する<br>
                一人一人の力が・・・未来を拓く
              </p>
              <div class="event-slider-links">
                <a href="/events/event-1/" class="event-slider-links__item link-arrow"><span>More</span></a>
                <a href="/form_entry_1/" class="event-slider-links__item link-arrow"><span>Entry</span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="l-section top-events">
      <div class="l-section__inner top-events__inner">
        <div class="top-events__calendar top-calendar">
          <h2 class="top-calendar__title">
            <svg role="image" class="svg-icon en">
              <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/icons.svg#title_calendar" />
            </svg>
            <span class="ja">イベントカレンダー</span>
          </h2>
          <div class="top-calendar__list top-calendar-list">
            <?php
            $args = array(
              'post_type' => 'calendar',
              'post_status' => 'publish',
              'posts_per_page' => 20,
              'meta_value' => date('Y-m-d'),// dateで現在の日時を取得。
              'meta_key'  => 'end_date',// カスタムフィールド「開催日（yyyy/mm/dd）」の値を取得
              'meta_compare' => '>='// meta_valueとmeta_keyを比較して未来の場合のみ表示
            );
            $the_query = new WP_Query($args); if($the_query->have_posts()):
            ?>
            <?php while ($the_query->have_posts()): $the_query->the_post(); ?>
            <?php
              $startDate = get_field('start_date');
              $startDateYear = date('Y.', strtotime($startDate));
              $startDateMonth = date('n.', strtotime($startDate));
              $startDateDate = date('j', strtotime($startDate));
              $startDateDayName = date(' D', strtotime($startDate));
              $endDate = get_field('end_date');
              $endDateYear = date('Y.', strtotime($endDate));
              $endDateMonth = date('n.', strtotime($endDate));
              $endDateDate = date('j', strtotime($endDate));
              $endDateDayName = date(' D', strtotime($endDate));
            ?>

            <a href="<?php the_permalink(); ?>" class="top-calendar-list__item">
              <p class="date">
                <!-- <span class="date-num"><?php the_field('time_short'); ?></span> -->
                <span class="date-num">
                  <?php
                    echo $startDateYear;
                    echo $startDateMonth;
                    echo $startDateDate;
                    if($startDate !== $endDate) {
                      echo '-';
                    }
                    if($startDateYear !== $endDateYear) {
                      echo $endDateYear;
                    }
                    if($startDateMonth !== $endDateMonth) {
                      echo $endDateMonth;
                    }
                    if($startDateDate !== $endDateDate) {
                      echo $endDateDate;
                    }
                  ?>
                </span>
              </p>
              <h3 class="title"><?php the_title(); ?></h3>
            </a>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
            <?php else: ?>
            <!-- <p>イベントがありません</p> -->
            <?php endif; ?>
          </div>
          <div class="top-calendar__more">
            <a href="/calendar/" class="link-arrow"><span>More</span></a>
          </div>
        </div>
        <ul class="top-events__list top-events-list">
          <li class="top-events-list__item">
            <h2 class="title">
              <span class="en">Event 2</span>
              <span class="ja">ワールドインテリアトレンドレポート<br>2017.5.26 FRI</span>
            </h2>
            <p class="desc">
              いち早く知ることができる国際展示会情報<br>
              最新デザイントレンド情報<br>
              デザイン諸国からのメッセージ
            </p>
            <h2 class="title">
              <span class="ja">ワークショップ<br>2017.5.28 SUN / 6.3 SAT </span>
            </h2>
            <p class="desc">
              デザイナーと一緒に楽しい手作り体験
            </p>
            <div class="top-events-links">
              <a href="/events/event-2/" class="top-events-links__item link-arrow is-s"><span>More</span></a>
              <a href="/form_join_2/" class="top-events-links__item link-arrow is-s"><span>Join</span></a>
            </div>
          </li>
          <li class="top-events-list__item">
            <h2 class="title">
              <span class="en">Event 3</span>
              <span class="ja">東京ミッドタウン・デザインハブ企画展<br>暮らしの未来「Next Generation - インテリアデザイナー展」<br>2017.5.26 FRI - 6.3 SAT</span>
            </h2>
            <p class="desc">
              次世代デザイナーの紹介及び作品展<br>
              豊かな暮らし・未来のためデザイナーは何を思考するのか
            </p>
            <h2 class="title">
              <span class="ja">連動企画展</span>
            </h2>
            <p class="desc">
              住まいのインテリアコーディネーションコンテスト『高校生部門受賞作品展』
            </p>
            <div class="top-events-links">
              <a href="/events/event-3/" class="top-events-links__item link-arrow is-s"><span>More</span></a>
            </div>
          </li>
          <li class="top-events-list__item">
            <h2 class="title">
              <span class="en">Event 4</span>
              <span class="ja">WORLD INTERIOR DAY / デザインシンポジウム<br>2017.5.27 SAT</span>
            </h2>
            <p class="desc">
              日本と世界がデザインでつながる日<br>
              世界規模の同時開催イベント IFI「WORLD INTERIORS DAY」<br>
              デザインシンポジウム
            </p>
            <div class="top-events-links">
              <a href="/events/event-4/" class="top-events-links__item link-arrow is-s"><span>More</span></a>
              <a href="/form_join_4/" class="top-events-links__item link-arrow is-s"><span>Join</span></a>
            </div>
          </li>
        </ul>
      </div>
    </section>
    <section class="l-section top-banners">
      <div class="l-section__inner top-banners__inner">
        <div class="top-banners-large">
          <?php
          $args = array(
            'post_type' => 'sponsor',
            'tax_query' => array(
              array(
                'taxonomy' => 'sponsor_taxonomy',
                'field' => 'slug',
                'terms' => 'sponsor_a'
              )
            ),
            'post_status' => 'publish',
            'posts_per_page' => -1
          );
          $the_query = new WP_Query($args); if($the_query->have_posts()):
          ?>
          <?php while ($the_query->have_posts()): $the_query->the_post(); ?>
          <a href="<?php the_permalink(); ?>" class="top-banners-large__item">
            <img src="<?php the_field('banner'); ?>" alt="<?php the_title(); ?>">
          </a>
          <?php endwhile; ?>
          <?php wp_reset_postdata(); ?>
          <?php else: ?>
          <?php endif; ?>
        </div>
        <div class="top-banners-small">
          <?php
          $args = array(
            'post_type' => 'sponsor',
            'tax_query' => array(
              array(
                'taxonomy' => 'sponsor_taxonomy',
                'field' => 'slug',
                'terms' => 'sponsor_b'
              )
            ),
            'post_status' => 'publish',
            'posts_per_page' => -1
          );
          $the_query = new WP_Query($args); if($the_query->have_posts()):
          ?>
          <?php while ($the_query->have_posts()): $the_query->the_post(); ?>
            <?php
              $no_page = get_field('no_page');
              if($no_page):
            ?>
              <span class="top-banners-small__item">
                <img src="<?php the_field('banner'); ?>" alt="<?php the_title(); ?>">
              </span>
            <?php else: ?>
              <a href="<?php the_permalink(); ?>" class="top-banners-small__item">
                <img src="<?php the_field('banner'); ?>" alt="<?php the_title(); ?>">
              </a>
            <?php endif; ?>
          <?php endwhile; ?>
          <?php wp_reset_postdata(); ?>
          <?php else: ?>
          <?php endif; ?>
        </div>
        <nav class="sponsors">
          <a href="http://www.jidp.or.jp/" target="_blank" class="sponsors__item"><img src="<?php echo get_template_directory_uri(); ?>/images/logo_jdp.png" class="js-retina-img" alt="JDP" width="225" height="30"></a>
          <a href="http://www.jid.or.jp/" target="_blank" class="sponsors__item"><img src="<?php echo get_template_directory_uri(); ?>/images/logo_jid.png" class="js-retina-img" alt="JID" width="288" height="23"></a>
          <a href="http://www.ifiworld.org/" target="_blank" class="sponsors__item"><img src="<?php echo get_template_directory_uri(); ?>/images/logo_ifi.png" class="js-retina-img" alt="IFI" width="191" height="24"></a>
        </nav>
      </div>
    </section>

<?php get_footer(); ?>
