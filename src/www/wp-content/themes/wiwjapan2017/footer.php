</main>
<footer class="l-footer footer" id="footer">
  <div class="l-footer__inner">
    <div class="footer-logo">
      <svg role="image" class="footer-logo__logo svg-icon">
        <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/icons.svg#logo" />
      </svg>
    </div>
    <nav class="footer-nav">
      <div class="footer-nav__col">
        <a href="/about" class="footer-nav__item">About</a>
        <a href="/events/" class="footer-nav__item">Events</a>
        <a href="/events/event-1/" class="footer-nav__item is-child">Event 1</a>
        <a href="/events/event-2/" class="footer-nav__item is-child">Event 2</a>
        <a href="/events/event-3/" class="footer-nav__item is-child">Event 3</a>
        <a href="/events/event-4/" class="footer-nav__item is-child">Event 4</a>
        <a href="/calendar/" class="footer-nav__item is-child">Event Calendar</a>
      </div>
      <div class="footer-nav__col">
        <a href="/news/" class="footer-nav__item">News</a>
        <a href="/2016/" class="footer-nav__item">Archive</a>
        <a href="/form_contact/" class="footer-nav__item">Contact</a>
      </div>
      <div class="footer-nav__col">
        <a href="https://www.facebook.com/World-Interiors-Day-1614209928791677/" class="footer-nav__item is-icon" target="_blank">
          <svg role="image" class="svg-icon is-facebook">
            <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/icons.svg#icon_facebook" />
          </svg>
        </a>

        <a href="/about_en" class="footer-nav__item">English</a>

      </div>
    </nav>
    <p class="footer-copyright">Copyright &copy; 2016 <br class="show-sp">World Interiors Week, All Rights Reserved.</p>
    <a href="#body" class="go-top js-smooth-scroll">
      <svg role="image" class="svg-icon is-go-top">
        <use xlink:href="<?php echo get_template_directory_uri(); ?>/images/icons.svg#icon_go-top" />
      </svg>
    </a>
  </div>
</footer>
</div>
<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/svg4everybody.min.js"></script>
<script>svg4everybody();</script>
<?php if(post_type_exists(calendar)): ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAg54N9LTZJpbLVUKn-THCNkEX3bTeSTbE"></script>
<script type="text/javascript">
  (function($) {

  /*
  *  new_map
  *
  *  This function will render a Google Map onto the selected jQuery element
  *
  *  @type  function
  *  @date  8/11/2013
  *  @since  4.3.0
  *
  *  @param  $el (jQuery element)
  *  @return  n/a
  */

  function new_map( $el ) {

    // var
    var $markers = $el.find('.marker');


    // vars
    var args = {
      zoom: 15,
      center: new google.maps.LatLng(0, 0),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      // scrollwheel: false,
      styles: [
        {
          "featureType": "all",
          "elementType": "all",
          "stylers": [
            {
              "saturation": -100
            },
            {
              "gamma": "1"
            }
          ]
        }
      ]
    };

    // create map
    var map = new google.maps.Map( $el[0], args);

    // add a markers reference
    map.markers = [];

    // add markers
    $markers.each(function(){
        add_marker( $(this), map );
    });

    // center map
    center_map( map );

    // return
    return map;
  }

  /*
  *  add_marker
  *
  *  This function will add a marker to the selected Google Map
  *
  *  @type  function
  *  @date  8/11/2013
  *  @since  4.3.0
  *
  *  @param  $marker (jQuery element)
  *  @param  map (Google Map object)
  *  @return  n/a
  */

  function add_marker( $marker, map ) {

    // var
    var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

    // create marker
    var marker = new google.maps.Marker({
      position  : latlng,
      map      : map,
      icon: '<?php echo get_template_directory_uri(); ?>/images/map_pin.png'
    });

    // add to array
    map.markers.push( marker );

    // if marker contains HTML, add it to an infoWindow
    if( $marker.html() )
    {
      // create info window
      var infowindow = new google.maps.InfoWindow({
        content    : $marker.html()
      });

      // show info window when marker is clicked
      google.maps.event.addListener(marker, 'click', function() {
        infowindow.open( map, marker );
      });
    }
  }

  /*
  *  center_map
  *
  *  This function will center the map, showing all markers attached to this map
  *
  *  @type  function
  *  @date  8/11/2013
  *  @since  4.3.0
  *
  *  @param  map (Google Map object)
  *  @return  n/a
  */

  function center_map( map ) {

    // vars
    var bounds = new google.maps.LatLngBounds();

    // loop through all markers and create bounds
    $.each( map.markers, function( i, marker ){

      var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
      bounds.extend( latlng );
    });

    // only 1 marker?
    if( map.markers.length == 1 )
    {
      // set center of map
        map.setCenter( bounds.getCenter() );
        map.setZoom( 15 );
    }
    else
    {
      // fit to bounds
      map.fitBounds( bounds );
    }
  }

  /*
  *  document ready
  *
  *  This function will render each map when the document is ready (page has loaded)
  *
  *  @type  function
  *  @date  8/11/2013
  *  @since  5.0.0
  *
  *  @param  n/a
  *  @return  n/a
  */
  // global var
  var map = null;

  $(document).ready(function(){

    $('.acf-map').each(function(){

      // create map
      map = new_map( $(this) );

    });
  });
  })(jQuery);
</script>
<?php endif; ?>
<?php wp_footer(); ?>
</body>
</html>
