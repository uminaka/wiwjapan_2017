<?php remove_filter ('the_content', 'wpautop'); ?>
<?php get_header(); ?>
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			the_content();

			// End of the loop.
		endwhile;
		?>
<?php get_footer(); ?>
