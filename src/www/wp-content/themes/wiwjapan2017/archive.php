<?php get_header(); ?>
<section class="l-section page-header">
	<div class="l-section__inner page-header__inner">
		<h1 class="title">News</h1>
	</div>
</section>
<section class="l-section page-contents">
	<div class="l-section__inner page-contents__inner">
		<div class="news-list">
			<?php if (have_posts()): ?>
			<?php while (have_posts()) : the_post(); ?>
			<a href="<?php the_permalink(); ?>" class="news-list__item news-list-item">
				<?php if (has_post_thumbnail()) : ?>
					<?php the_post_thumbnail('thumbnail', array( 'class' => 'news-list-item__image' ) ); ?>
				<?php else : ?>
					<img src="<?php bloginfo('template_url'); ?>/images/news_placeholder.png" alt="" class="news-list-item__image">
				<?php endif ; ?>
				<span class="news-list-item__text">
					<span class="date"><?php echo get_post_time('Y.m.d D'); ?></span>
					<span class="title"><?php echo get_the_title(); ?></span>
				</span>
			</a>
			<?php endwhile; ?>
			<?php else: ?>
			<?php endif; ?>
		</div>
		<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
	</div>
</section>
<?php get_footer(); ?>
